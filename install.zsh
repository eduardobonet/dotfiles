# Better cd
brew install zoxide
eval "$(zoxide init zsh --cmd cd)"

# Better history search
brew tap cantino/mcfly
brew install mcfly
eval "$(mcfly init zsh)"
