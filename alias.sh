# General
alias a='cat ~/git_tree/dotfiles/alias.sh' 

# Python
alias vmake="python3 -m virtualenv .venv && source .venv/bin/activate"
alias vact="source .venv/bin/activate"

# Git
alias np='git push -u origin $(git rev-parse --symbolic-full-name --abbrev-ref HEAD)'
alias gm='git commit -a -m'
alias dofix='git add -A && git commit --amend --no-edit'
alias cf='git diff --name-only $(git merge-base master $(git rev-parse --abbrev-ref HEAD))'
alias copymsg='git log --format=%B -n 1 | pbcopy'

# Gitlab
alias gld='cd ~/git_tree/gitlab-development-kit/gitlab'
alias gls='gdk start && gdk stop rails-web'
alias gm='git checkout db/structure.sql'
alias gstg='tsh ssh rails-ro@console-ro-01-sv-gstg'

alias prerb='git diff --name-only | grep .rb | xargs bundle exec rubocop --auto-correct-all'
alias fixrb='git show HEAD --name-only --pretty="format:" | grep .rb | xargs rubocop --auto-correct-all'
alias ajest='cf | grep "_spec.js" | xargs -n 10000 yarn jest'
alias atests='cf | awk  "/.rb$/ && !/spec\/factories/" | sed -e "s/^app\/controllers/spec\/requests/" -e "s/^app/spec/" -e  "/_spec.rb$/!s/.rb$/_spec.rb/" -e "s/^lib/spec\/lib/" | xargs -n 100000 bundle exec rspec -f documentation'

mr_failed_test() {
  local project_path=$(git remote get-url origin | perl -ne '/:(.*?)(?:\.git)?$/ and print $1')
  local mr_iid="$1"
  local branch=$(git rev-parse --abbrev-ref HEAD)

  if [ "$mr_iid" = "" -a "$branch" != "master" ]
  then
    local escpaed_project_path=${project_path//\//%2f}
    mr_iid=$(curl -sG --data-urlencode "source_branch=${branch}" "https://gitlab.com/api/v4/projects/$escpaed_project_path/merge_requests" | jq -j '.[0].iid')
  fi

  if [ "$mr_iid" = "" ]
  then
    echo "MR IID is required for '$branch'"
    return 1
  fi

  echo "# Fetching from https://gitlab.com/${project_path}/-/merge_requests/${mr_iid}/test_reports.json" >&2
  curl -s "https://gitlab.com/${project_path}/-/merge_requests/${mr_iid}/test_reports.json" | jq -r '.suites[]? | [{file: (.new_failures[].file | sub("./"; ""))}] | unique | .[].file'
}

mr() {
  local project_path=$(git remote get-url origin | perl -ne '/:(.*?)(?:\.git)?$/ and print $1')
  local escpaed_project_path=${project_path//\//%2f}
  local branch=$(git rev-parse --abbrev-ref HEAD)
  local mr_iid=$(curl -sG --data-urlencode "source_branch=${branch}" "https://gitlab.com/api/v4/projects/$escpaed_project_path/merge_requests" | jq -j '.[0].iid')
  
  if [ "$mr_iid" = "" ]
  then
    echo "No MR found"
    return 1
  fi

  echo "https://gitlab.com/$project_path/merge_requests/$mr_iid"
}
