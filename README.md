# Personal Dotfiles


## Setup

Linking the files:

```
ln -s $(pwd)/vimrc ~/.vimrc
```

Add to .zshrc:

```bash
echo "source $(pwd)/alias.sh" > ~/.zshrc
source ~/.zshrc
```
